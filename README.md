# Desafio Jack Experts

Projeto criado com intuito de ser o repositório do desafio de estágio para a empresa Jack Experts. Criação de um container Docker para subir uma página HTML.


## Como rodar o projeto

Para rodar o projeto, primeiro, você precisa construir a imagem do Docker. Para ser contruida, rode o comando: 
```
docker build -t desafio-jack-experts:v1 .
```

Uma vez constrtuida, você irá precisará rodar a imagem. Para rodar a imagem, digite o comando:
```
docker run -p 80:80 desafio-jack-experts:v1
```

Assim que o container estiver rodando, você poderá acessar o conteúdo do desafio através de qualquer navegador no endereço:
```
localhost:80
```


#### Observação

Caso você tenha algum problema de permissão ao rodar os comando docker, rode os comandos como administrador.
